<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp34');

/** MySQL database username */
define('DB_USER', 'wp34');

/** MySQL database password */
define('DB_PASSWORD', 'p-ZF3x6S5.');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rcrctqnudeuctnktpummuumohd3fz6h229yo4noykf96jswvt4qdxu4wovxfcdei');
define('SECURE_AUTH_KEY',  'ywb22tw87apa3vcsjeukbuanel6exobks14qcdrn1xadfsveg6jym3yhvhapmhoz');
define('LOGGED_IN_KEY',    'oknwkgbiet3o7og8ssdaybnvbqdytg2hnxoadnoz6nygxhhefqyyfgdsnmnv4yaq');
define('NONCE_KEY',        'gcrblt4hcxibxowqhnihaupj3ft2gs9rj3g7utofjaazpxkam6kmbj0rqzlvmsl3');
define('AUTH_SALT',        'vios6xgdco0d1ngqejsiqy81pkmzgmejs12w1eiyhmyeuy1le5czej6dax4kbiwj');
define('SECURE_AUTH_SALT', '2nwerrajurd5m02du4rlfxengxwmkwt1dfymhxqy0vy7czmaredpqmo2yybrnp3o');
define('LOGGED_IN_SALT',   'ayaljyvgsvqwz7ifdjfxvaqd9wil6ywwi7tg8qwwhayfagn7pio0exzlkrbukylg');
define('NONCE_SALT',       'ps0dembnynsps2zc8hkdjrz2w0oxufmcijaspym9kdvqbqnrgdx4zbkuajjwko3j');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpep_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
